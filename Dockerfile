FROM java:8-jdk

COPY ./build/libs/cicd-sample-*.jar /opt/cicd-sample/
 
EXPOSE 8080
RUN  ls -d -1 /opt/cicd-sample/*.* | xargs -I {} echo java -jar {} > /entrypoint.sh \
	&& chmod u+x /entrypoint.sh
CMD /entrypoint.sh
